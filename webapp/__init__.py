from os import environ
from flask import Flask
from flask_migrate import Migrate

from flask_sqlalchemy import SQLAlchemy


class Config:
    FLASK_DEBUG = environ.get('FLASK_DEBUG', '1')

    SQLALCHEMY_DB_URI = environ.get('SQLALCHEMY_DB_URI',
                                    'postgresql://postgres:postgres@localhost/webapp')
    SQLALCHEMY_TRACK_NOTIFICATIONS = environ.get('SQLALCHEMY_TRACK_NOTIFICATIONS', False)
    SQLALCHEMY_ECHO = environ.get('SQLALCHEMY_ECHO', True)


app = Flask(__name__, template_folder='templates')

app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

from webapp import models
from webapp import routes
