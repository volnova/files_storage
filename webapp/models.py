from webapp import db


class Post(db.Model):
    """Model for user accounts."""

    __tablename__ = 'posts'
    id = db.Column(db.Integer,
                   primary_key=True)
    title = db.Column(db.String(64),
                      index=True,
                      unique=True,
                      nullable=False)
    description = db.Column(db.String(80))
    likes = db.Column(db.Integer, default=0)

    def __repr__(self):
        return f'Post "{self.title}"'
