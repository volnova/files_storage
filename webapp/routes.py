from flask import jsonify, request
from flask import send_file, render_template, abort
from datetime import datetime

from webapp import app
from webapp import db
from webapp.models import Post


@app.route('/create/<song_name>')
def create_song(song_name):
    with open('songs.txt', 'a') as file:
        file.write(song_name + '\n')
    return f'Created song {song_name}'


@app.route('/list_')
def return_songs_list():
    song_dict = {}
    with open('songs.txt', 'r') as file:
        for idx, song in enumerate(file):
            song_dict[idx+1] = song.replace('\n', '')

    return jsonify(song_dict)


@app.route('/song/<number>')
def return_song_name(number):
    with open('songs.txt', 'r') as file:
        try:
            return file.readlines()[(int(number))-1].replace('_', ' ').upper()
        except IndexError:
            return f'There is no song with number {number}'


@app.route('/download_list')
def return_songs_file():
    return send_file('../songs.txt')


@app.route('/time')
def return_time():
    return render_template('time.html', time=datetime.now())


@app.errorhandler(404)
def page_not_found(error):
    return render_template('page_not_found.html'), 404


@app.route('/time_')
def return_time_():
    abort(404)


@app.route('/add')
def add():
    title = request.args.get('title')
    if title:
        post = Post(title=title)
        db.session.add(post)
        db.session.commit()
        return f'{post} successfully created!'
    else:
        return 'Nothing to create :('


@app.route('/list')
def list_posts():
    posts = Post.query.all()
    posts = {p.title: p.description for p in posts}
    return jsonify(posts)
